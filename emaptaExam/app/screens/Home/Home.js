import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  Linking,
  Platform,
  Dimensions,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import moment from 'moment';
import {isEmpty} from 'lodash';
const {width, height} = Dimensions.get('window');

import {getArticles} from '../../store/actions';
import styles from './Home.style';

class Home extends Component {
  state = {};

  componentDidMount() {
    const {onGetArticles} = this.props;
    onGetArticles();
  }

  render() {
    const {articles, navigation, onGetArticles} = this.props;
    return (
      <View style={styles.container}>
        {/* HEADER */}
        <SafeAreaView style={styles.header}>
          <View style={styles.upperHeader}>
            <View style={styles.userHeaderNameWrap}>
              <Text numberOfLines={2} style={styles.userHeaderName}>
                Articles
              </Text>
            </View>
          </View>
        </SafeAreaView>
        {/* BODY */}
        <ScrollView
          refreshing={false}
          onRefresh={() => onGetArticles()}
          style={styles.body}>
          {!isEmpty(articles) &&
            articles.map((article) => {
              console.log(article.urlToImage);
              return (
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate('Details', {
                      article: article,
                    })
                  }
                  style={{marginTop: 20}}>
                  <Image
                    source={{uri: article.urlToImage}}
                    resizeMode="cover"
                    style={styles.articleImage}
                  />
                  <View style={{height: height / 4, marginVertical: 20}}>
                    <Text numberOfLines={2} style={styles.title}>
                      {article.title}
                    </Text>
                    <Text numberOfLines={2} style={styles.date}>
                      {moment(article.publishedAt).format('LL')}
                    </Text>
                    <Text numberOfLines={2} style={styles.author}>
                      by {article.author}
                    </Text>
                    <Text style={styles.description}>
                      {article.description}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            })}
        </ScrollView>
        {/* FOOTER */}
        <SafeAreaView style={styles.footer}>
          <Text style={styles.ads}>Sample Ads</Text>
          <Text style={styles.noe}>Noe Fabellon</Text>
        </SafeAreaView>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  articles: state.article.articles,
});

const mapDispatchToProps = (dispatch) => ({
  onGetArticles: (data) => dispatch(getArticles(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
