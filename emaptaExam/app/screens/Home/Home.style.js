import {StyleSheet, Dimensions, Platform} from 'react-native';
import {normalize} from 'react-native-elements';

const {width, height} = Dimensions.get('window');

const standardFontSize = Platform.OS === 'ios' ? normalize(13) : normalize(15);

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    flex: 0.2,
    width,
    backgroundColor: '#fe9a2e',
  },
  body: {
    flex: height > 650 ? 0.7 : 0.65,
    width,
  },
  footer: {
    flex: height > 650 ? 0.1 : 0.15,
    backgroundColor: '#fe9a2e',
    width,
    justifyContent: 'center',
  },
  upperHeader: {
    flex: 0.5,
    width,
    alignItems: 'center',
    flexDirection: 'row',
  },
  userHeaderNameWrap: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  userHeaderName: {
    fontSize: Platform.OS === 'ios' ? normalize(18) : normalize(21),
    textAlign: 'center',
    color: 'black',
  },
  title: {
    fontSize: Platform.OS === 'ios' ? normalize(14) : normalize(17),
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold',
  },
  author: {
    fontSize: Platform.OS === 'ios' ? normalize(12) : normalize(15),
    textAlign: 'center',
    color: 'black',
  },
  date: {
    fontSize: Platform.OS === 'ios' ? normalize(12) : normalize(15),
    textAlign: 'center',
    color: 'black',
    marginVertical: 10,
  },
  articleImage: {
    height: height / 3,
  },
  description: {
    fontSize: Platform.OS === 'ios' ? normalize(12) : normalize(15),
    color: 'black',
    marginVertical: 30,
    marginHorizontal: 20,
  },
  ads: {
    color: 'black',
    textAlign: 'center',
    fontSize: Platform.OS === 'ios' ? normalize(18) : normalize(21),
  },
  noe: {
    color: 'black',
    textAlign: 'center',
    fontSize: Platform.OS === 'ios' ? normalize(12) : normalize(15),
  },
});
