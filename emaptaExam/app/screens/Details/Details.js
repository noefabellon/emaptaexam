import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  Linking,
  Platform,
  ScrollView,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import {get} from 'lodash';
import moment from 'moment';

const {width, height} = Dimensions.get('window');
import {BackButton} from '../../components';
import styles from './Details.style';

class Details extends Component {
  state = {};

  componentDidMount() {}

  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        {/* HEADER */}
        <SafeAreaView style={styles.header}>
          <View style={styles.upperHeader}>
            <BackButton onPress={() => navigation.goBack()} />
            <View style={styles.userHeaderNameWrap}>
              <Text numberOfLines={2} style={styles.userHeaderName}>
                Details
              </Text>
            </View>
          </View>
        </SafeAreaView>
        {/* BODY */}
        <ScrollView style={styles.body}>
          <Image
            source={{uri: get(navigation, 'state.params.article.urlToImage')}}
            resizeMode="cover"
            style={styles.articleImage}
          />
          <View style={{marginVertical: 20}}>
            <TouchableOpacity
              onPress={() =>
                Linking.openURL(get(navigation, 'state.params.article.url'))
              }>
              <Text numberOfLines={2} style={styles.title}>
                {get(navigation, 'state.params.article.title')}
              </Text>
            </TouchableOpacity>
            <Text numberOfLines={2} style={styles.date}>
              {moment(
                get(navigation, 'state.params.article.publishedAt'),
              ).format('LL')}
            </Text>
            <Text numberOfLines={2} style={styles.author}>
              by {get(navigation, 'state.params.article.author')}
            </Text>
            <Text style={styles.description}>
              {get(navigation, 'state.params.article.content')}
            </Text>
          </View>
        </ScrollView>
        {/* FOOTER */}
        <SafeAreaView style={styles.footer}>
          <Text style={styles.ads}>Sample Ads</Text>
          <Text style={styles.noe}>Noe Fabellon</Text>
        </SafeAreaView>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  // currentUser: state.user.userData,
  // vehicles: get(state, `vehicle.vehicles.${state.user.userData.accountID}`),
  // ads: state.ads.ads,
  // banners: state.ads.banners,
  // validRFIDs: state.vehicle.checkAllRFIDResults,
  // isFetchingVehicles: state.vehicle.isFetchingVehicles,
  // isFetchingAds: state.ads.isFetchingAds,
  // isFetchingPopupAds: state.ads.isFetchingPopupAds,
  // isFetchingBanners: state.ads.isFetchingBanners,
  // isCheckingAllRFID: state.vehicle.isCheckingAllRFID,
  // isSendingVerification: state.auth.isSendingVerification,
  // isUpdatingUserInfo: state.user.isUpdatingUserInfo,
  // popupads: state.ads.popupads,
});

const mapDispatchToProps = (dispatch) => ({
  // onFetchVehicles: (data) => dispatch(getVehicles(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Details);
