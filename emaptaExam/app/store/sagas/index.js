import {fork, all} from 'redux-saga/effects';

import {getArticlesWatcher, getArticleDetailsWatcher} from './article';

function* rootSaga() {
  /* ARTICLES */
  yield all([fork(getArticlesWatcher)]);
  yield all([fork(getArticleDetailsWatcher)]);
}

export default rootSaga;
