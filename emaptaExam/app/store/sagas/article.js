import {takeLatest, call, put} from 'redux-saga/effects';

import {
  GET_ARTICLES_REQUEST,
  GET_ARTICLES_SUCCESS,
  GET_ARTICLES_FAILURE,
  GET_ARTICLE_DETAILS_REQUEST,
  GET_ARTICLE_DETAILS_SUCCESS,
  GET_ARTICLE_DETAILS_FAILURE,
} from '../actions/actionTypes';

import {getArticles, getArticleDetails} from '../api/article';

/* GET ARTICLES */
function* getArticlesWorker(action) {
  try {
    const result = yield call(getArticles);
    // dispatch success action
    if (result.ok) {
      yield put({type: GET_ARTICLES_SUCCESS, payload: result});
    } else {
      yield put({type: GET_ARTICLES_FAILURE, payload: result});
    }
  } catch (error) {
    // dispatch failure action
    yield put({type: GET_ARTICLES_FAILURE, error});
  }
}

export function* getArticlesWatcher() {
  yield takeLatest(GET_ARTICLES_REQUEST, getArticlesWorker);
}

/* GET ARTICLE_DETAILS */
function* getArticleDetailsWorker(action) {
  try {
    const result = yield call(getArticleDetails);
    // dispatch success action
    if (result.ok) {
      yield put({type: GET_ARTICLE_DETAILS_SUCCESS, payload: result});
    } else {
      yield put({type: GET_ARTICLE_DETAILS_FAILURE, payload: result});
    }
  } catch (error) {
    // dispatch failure action
    yield put({type: GET_ARTICLE_DETAILS_FAILURE, error});
  }
}

export function* getArticleDetailsWatcher() {
  yield takeLatest(GET_ARTICLE_DETAILS_REQUEST, getArticleDetailsWorker);
}
