import {createStore, compose, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import AsyncStorage from '@react-native-community/async-storage';
import {createLogger} from 'redux-logger';
import {createMigrate, persistStore, persistReducer} from 'redux-persist';

import rootReducer from './reducers';
import rootSaga from './sagas';

const configureStore = initialState => {
  const sagaMiddleware = createSagaMiddleware();

  const composeEnhancers = compose;
  const persistConfig = {
    key: 'root',
    version: 1,
    storage: AsyncStorage,
    // migrate: createMigrate(migrations, {debug: false}),
    // blacklist: ['nav', 'buddyInvite', 'buddyRequest'],
  };
  const persistedReducer = persistReducer(persistConfig, rootReducer);
  if (__DEV__) {
    const loggerMiddleware = createLogger({collapsed: true});
    const store = {
      ...createStore(
        persistedReducer,
        initialState,
        composeEnhancers(applyMiddleware(sagaMiddleware, loggerMiddleware)),
      ),
      runSaga: sagaMiddleware.run(rootSaga),
    };
    const persistor = persistStore(store);
    return {store, persistor};
  }

  const store = {
    ...createStore(
      persistedReducer,
      initialState,
      composeEnhancers(applyMiddleware(sagaMiddleware)),
    ),
    runSaga: sagaMiddleware.run(rootSaga),
  };
  const persistor = persistStore(store);
  return {store, persistor};
};

export default configureStore;
