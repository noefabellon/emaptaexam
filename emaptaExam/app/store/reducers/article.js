import {get, isObject, isString} from 'lodash';
import {
  GET_ARTICLES_REQUEST,
  GET_ARTICLES_FAILURE,
  GET_ARTICLES_SUCCESS,
  GET_ARTICLE_DETAILS_REQUEST,
  GET_ARTICLE_DETAILS_FAILURE,
  GET_ARTICLE_DETAILS_SUCCESS,
} from '../actions/actionTypes';

const initialState = {
  isFetchingArticles: false,
  isFetchingArticleDetails: false,
  hasError: false,
  error: {
    message: '',
  },
};

const article = (state = initialState, action) => {
  switch (action.type) {
    case GET_ARTICLES_REQUEST:
      return {
        ...state,
        isFetchingArticles: true,
        hasError: false,
        error: {message: ''},
      };

    case GET_ARTICLE_DETAILS_REQUEST:
      return {
        ...state,
        isFetchingArticleDetails: true,
        hasError: false,
        error: {message: ''},
      };

    case GET_ARTICLES_SUCCESS:
      return {
        ...state,
        isFetchingArticles: false,
        hasError: false,
        articles: get(action, 'payload.articles'),
      };

    case GET_ARTICLE_DETAILS_SUCCESS:
      return {
        ...state,
        isFetchingArticleDetails: false,
        hasError: false,
        banners: Object.values(get(action, 'payload')).filter((item) =>
          isString(item),
        ),
      };

    case GET_ARTICLES_FAILURE:
      return {
        ...state,
        isFetchingArticles: false,
        hasError: true,
        error: get(action, 'payload'),
      };

    case GET_ARTICLE_DETAILS_FAILURE:
      return {
        ...state,
        isFetchingArticleDetails: false,
        hasError: true,
        error: get(action, 'payload'),
      };

    default:
      return state;
  }
};

export default article;
