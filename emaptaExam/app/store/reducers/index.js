import {combineReducers} from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import {persistReducer} from 'redux-persist';

import article from './article';

const articlePersistConfig = {
  key: 'article',
  storage: AsyncStorage,
};

const rootReducer = combineReducers({
  article: persistReducer(articlePersistConfig, article),
});

export default rootReducer;
