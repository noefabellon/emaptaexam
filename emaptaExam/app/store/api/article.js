import ajax from './ajax';
import {API, APIKEY} from '../../constants/api';
import moment from 'moment';

export const getArticles = () => {
  let currentDate = new Date();
  const date = moment(currentDate).format('YYYY-MM-DD');
  const url = `${API}/everything?q=bitcoin&from=${date}&sortBy=publishedAt&apiKey=${APIKEY}`;
  return ajax(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  });
};

export const getArticleDetails = () => {
  const url = `${API}/config/banner.json`;
  return ajax(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  });
};
