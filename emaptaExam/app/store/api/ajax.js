const ajax = (url, opts = {}, {token} = {}) => {
  const headers = (() => (opts ? {...opts.headers} : {}))();
  const auth = `Bearer ${token}`;

  if (token) headers.Authorization = auth; // eslint-disable-line fp/no-mutation

  return fetch(url, { // eslint-disable-line
    ...opts,
    headers,
  })
    .then(response => {
      const {status, ok} = response;
      return response.text().then(text => {
        const parsed = text
          ? {...JSON.parse(text), respStatus: status, ok}
          : {respStatus: status, ok};
        return parsed;
      });
    })
    .then(resp => {
      const {status} = resp;
      if (status === 401) {
        return {
          ...resp,
          message: `Your session just expired, bud. Just try logging in again. If you encounter any problems, contact us at support@bliimo.com or send us a message at out Facebook Page.`,
        };
      }
      return resp;
    })
    .catch(error => {
      if (error.message.match(/Network request failed/)) {
        return {
          status: 503,
          message:
            'Service Unavailable. Please check your internet connection.',
        };
      }
      return error;
    });
};

export default ajax;
