import {GET_ARTICLES_REQUEST, GET_ARTICLE_DETAILS_REQUEST} from './actionTypes';

export const getArticles = () => ({
  type: GET_ARTICLES_REQUEST,
});

export const getArticleDetails = () => ({
  type: GET_ARTICLE_DETAILS_REQUEST,
});
