import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Home from './screens/Home/Home';
import Details from './screens/Details/Details';

export const ScreensStack = createStackNavigator(
  {
    Home: {screen: Home},
    Details: {screen: Details},
  },
  {
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false,
      headerStyle: {
        backgroundColor: '#fff',
      },
    },
  },
);

export const AppNavigator = createSwitchNavigator(
  {
    ScreensStack: ScreensStack,
  },
  {
    defaultNavigationOptions: {
      header: null,
      gesturesEnabled: false,
      headerStyle: {
        backgroundColor: '#fff',
      },
    },
  },
);

export default createAppContainer(AppNavigator);
