import React from 'react';

import {TouchableOpacity, Image, StyleSheet} from 'react-native';

import back_btn_img from '../assets/images/back-icon.png';

const BackButton = ({onPress}) => (
  <TouchableOpacity onPress={onPress} style={styles.touchableArea}>
    <Image
      source={back_btn_img}
      resizeMode="contain"
      style={styles.backBtnImg}
    />
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  backBtnImg: {
    height: 25,
    width: 25,
  },
  touchableArea: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 1000,
  },
});

export default BackButton;
